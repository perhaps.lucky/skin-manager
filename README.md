# SkinManager

## Disclaimer

Some features are discontinued. Right now it is just supposed to import skins into newest version of MonoPanda.

For MonoPanda 1.x.x use branch `MonoPanda-1.x.x`

## Description

SkinManager is a tool dedicated to adding and modyfing external skins in Spine assets in MonoPanda projects.

It started as a personal tool (so code quality is kinda bad + it's clunky at points), but eventually I decided it might be a nice tool for modding or even for other MonoPanda projects (if there will ever be any :D).


## Warning

**Always use git or other similar tool to verify changes in your project**. In case you don't do that already. But yeah, always use it and verify changes. SkinManager edits json files as well as removes and adds files to content folder. It's easy to remove something accidently (and there is no ctrl+z).

## Starting up

You can download built program from release page.

Once you turn it on, it will ask you to point to your project folder. Select main project folder (where Content folder is). Worth noting that it works both on MonoPanda project and built game project. File system should be exactly the same so it doesn't really matter if you are working on mod or main project.

Once project loads, you will see 3 tabs in top part. Let's start with settings cause it has some important bits.

## Settings

If project is fresh, you will probably see many empty fields here. Not all of them are necessary, but some are useful. Well, all should be.

In top part you have path to currently opened project and button to open another. I think that goes without explanation.

Below there is a list where you can select active mod. You can also refresh the list if you add new mod. **Quick guide to adding new mod**: in `Content/Mods/` you need to add new folder. Inside create `mod-info.json` file. Put `{"FullName":"<your mod name>"}` there. Refresh and it should work. You can read more about mods in [MonoPanda documentation](https://gitlab.com/perhaps.lucky/monopanda/-/blob/master/Docs/chapters/Basics/Managers/Mods.md) (altho it's kinda messy ~~from moder point of view~~ it's messy af)

Now the fun part, two different settings sections.

### Current project settings

These settings are meant for project on which you are currently working. By default they will be stored in `<project directory>/Content/MonoPanda/skin-manager.ini`. You most likely want these project saved in your repository so others have them as well!

**Available side prefixes** - add side prefixes, separated by `,`. If you go to `Skin Manager` tab, on the right you see a `Side` selector. Once you switch it to any of availables sides, all slots **without** that prefix in name will be hidden in preview. This forces you to have project in which every slots has a prefix of which "side" he belongs to. But it works nicely if you have let's say 3 side character project. **This is purely preview feature, it will not affect your project.**

**Available color channel suffixes** - similar to one above, but here we operate on suffixes. You can change color in preview in each of channels to see how your skin looks.

**Imported skin default location** - location to which skins will be imported by default (folder will open in location picker so it is just to smooth process up)

**Auto mesh map** - this field should contain pairs in format `<part of name>:<mesh copy>` separtated by `;`. On import, each slot name will be check if it contains any of keys in map. If match is found, skin will import mesh data from other skin (imported mesh should be in base spine project).

**Multi slot map** - this field can be used to map a single frame from spritesheet to multiple skin slots (during skin import). The format is `<frame name>:<list of slots that should use that frame>`. Items on the list should be separated by `,`, while map records should be separated by `;`. For example: `leg:leg-right,leg-left;foot:foot-right,foot-left` will use `leg` and `foot` frames on both legs of the character (assuming names of slots are the same as in example).

### Skin Manager settings

These settings settings are not project related (obviously as opposite to previous part). They are mostly meant to speed up using the skin manager by automatically taking care of repeatiting parts.

**After start open project** - what it says, instead of pop-up to select project, SkinManager will automatically open this project.

**Start spine project Id** - id of spine project which will be automatically selected

**Enable skins** - list of skin names, separated by `,` which will be automatically selected as enabled after start of program.

**Assets path** - path which will be opened in file selector after you press `Import` button.

## Skin Manager tab

Main part of this tool. Allows preview and adjustment of skins. UI is separated in two parts. On left there are settings of skins, basically everything that will be saved. Right part are preview features.

### External skins

External skin list is list of skins **added by currently active mod** (if you work in context of project, main mod should be the active mod). You can enable and disable skins by double clicking them or selecting and pressing `Enable/Disable` button.

#### Import

You can import spritesheets created with [TexturePacker](https://www.codeandweb.com/texturepacker). There few rules to follow in naming:

* exported format is `json-array` and png
* both files should be in the same directory (default behaviour of TexturePacker)
* `filename` in exported spritesheet json should match its slot name in spine project
* name of exported file will be used as id of external skin as well as id of added content item

What import exactly does is:

1) Copy exported spritesheet (json and png file)
2) Add new content item with Id equal to name of imported file
3) Add new external skin with each attachment pointing to sprite with the same name + automatically set mesh based on `Auto mesh map` setting

#### Remove

Upon pressing remove, tool will ask if you want to delete content item as well. Pressing `Remove with content item` will remove spritesheet content item as well as files in mod directory. Use with caution.

### External skin settings

In this section you can adjust each of attachments. Select attachment from the list to begin.

Note that attachment linked with mesh can't be adjusted. It's because they copy 100% of settings of that mesh during runtime. Mesh links can be only created to skins that are in base spine project.

Sometimes the skin doesn't refresh on it's own. There are two things to try:

* disable and enable skin
* enable some core skin (for some reason in my project I have to enable `base` skin, no idea what it changes, it just doesn't work without it)

### Core skins

This is a list of skins from spine project. List is always showing project loaded from main mod, so if it got overrided by some other mod, changes will be not visible here (that feature would be difficult to implement).

You can enable/disable skins for preview reasons.

### Side

List of sides based on `Available side prefixes` setting. Selecting one sides, hides all slots that do not have matching prefix in their name.

### Animation

You can select animation from spine project and it will be played in preview. See how skin acts in action.

### Color channels

List of channels is based on `Available color channel suffixes` setting. You can select one of channels and select color in right part. All slots with matching suffix in their name will have changed color in preview.

## Content item tab

This tab allows adding, editing and removing content items added by active mod. You will not see core mod items here unless core mod is active. Your mod however can override core mods by having same id as item in core (it also works for any mod that is loaded before your mod).

Using delete will remove content item and matching files from mod folder.

In import note how `File to import` shows which file you should select. If selected item type requires more than one file, other files should be in the same directory, with same name. After import to `Save location`, they will also have same file name.

## Importing gimp projects

This feature requires additional changes to structure of your files. It uses external bat file that will do export from gimp .xcf file, to spritesheet. Check `example_gimp_import` for details.


