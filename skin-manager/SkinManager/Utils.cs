﻿using System.IO;

namespace SkinManager {
  public class Utils {
    public static bool IsDirectoryWithinAnother(string checkedDirectory, string parentDirectory) {
      var di1 = new DirectoryInfo(parentDirectory);
      var di2 = new DirectoryInfo(checkedDirectory);

      if (di1.FullName.Equals(di2.FullName)) {
        return true;
      }

      bool isParent = false;
      while (di2.Parent != null) {
        if (di2.Parent.FullName == di1.FullName) {
          isParent = true;
          break;
        }

        di2 = di2.Parent;
      }

      return isParent;
    }
  }
}