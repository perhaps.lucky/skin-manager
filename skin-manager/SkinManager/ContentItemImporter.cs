﻿using MonoPanda.Content;
using MonoPanda.Logger;

using System;
using System.IO;

namespace SkinManager {
  public class ContentItemImporter {
    private string id;
    private string targetFolder;
    private string importTarget;
    private ContentItemType contentItemType;
    private string fileName;

    public static bool Import(string id, string targetFolder, string importTarget, ContentItemType contentItemType) {
      var instance = new ContentItemImporter(id, targetFolder, importTarget, contentItemType);
      return instance.tryImport();
    }

    private ContentItemImporter(string id, string targetFolder, string importTarget, ContentItemType contentItemType) {
      this.id = id;
      this.targetFolder = targetFolder;
      this.importTarget = importTarget;
      this.contentItemType = contentItemType;
      this.fileName = Path.GetFileName(importTarget);
    }

    private bool tryImport() {
      try {
        import();
      } catch (Exception e) {
        Log.log(LogCategory.SkinImporter, LogLevel.Error, $"Content item importer failed. Unknown exception: {e}");
        return false;
      }

      Log.log(LogCategory.SkinImporter, LogLevel.Info, $"Content item import successful");
      return true;
    }

    private void import() {
      copyFiles();
    }

    private void copyFiles() {
      copyMainFile();
      copyAdditionalFiles();
      addContentItem();
    }

    private void copyMainFile() {
      File.Copy(importTarget,
        $"{Project.GetActiveProjectPath()}{Project.ModsPath}{Project.ActiveMod}/{targetFolder}/{fileName}");
    }

    private void copyAdditionalFiles() {
      switch (contentItemType) {
        case ContentItemType.SpineAsset:
          copyWithReplace(".atlas", ".json");
          copyWithReplace(".atlas", ".png");
          break;
        case ContentItemType.BitmapFont:
          copyWithReplace(".fnt", ".png");
          break;
        case ContentItemType.SpriteSheet:
          copyWithReplace(".png", ".json");
          break;
      }
    }

    private void copyWithReplace(string from, string to) {
      File.Copy(importTarget.Replace(from, to),
        $"{Project.GetActiveProjectPath()}{Project.ModsPath}{Project.ActiveMod}/{targetFolder}/{fileName.Replace(from, to)}");
    }

    private void addContentItem() {
      var contentItem = new ContentItem {
        Id = id, FilePath = $"{targetFolder}/{fileName}", ItemType = contentItemType
      };
      Project.GetContentItems().Add(contentItem);
      Project.SaveContentItems();
    }
  }
}