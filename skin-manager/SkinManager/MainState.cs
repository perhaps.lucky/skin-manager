﻿using IniParser.Model;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using MonoPanda;
using MonoPanda.Components;
using MonoPanda.Configuration;
using MonoPanda.Content;
using MonoPanda.ECS;
using MonoPanda.ECS.Components.DrawingComponent;
using MonoPanda.Input;
using MonoPanda.Logger;
using MonoPanda.Spine.MonoPanda;
using MonoPanda.States;
using MonoPanda.Systems;
using MonoPanda.UserSettings;
using MonoPanda.Utils;

using Myra;
using Myra.Graphics2D.UI;
using Myra.Graphics2D.UI.File;

using Spine;

using System;
using System.Collections.Generic;

namespace SkinManager {
  public class MainState : State {
    private Desktop desktop;
    private UI ui;

    private EntityComponentSystem ecs;

    private Entity entity;
    private SpineComponent spineComponent;
    private DrawComponent drawComponent;

    public MainState(string id) : base(id) {
    }

    public override void Initialize() {
      MyraEnvironment.Game = GameMain.GetInstance();
      desktop = new Desktop();
      ecs = EntityComponentSystem.LoadFromJson("ecs.json");
      EntityFactory.CreateInstance<EntityFactory>("ef.json", ecs);

      if (string.IsNullOrEmpty(Settings.SkinManager.StartProjectPath)) {
        openOpenProjectDialog();
      } else {
        try {
          openProject(Settings.SkinManager.StartProjectPath);
        } catch (Exception e) {
          Log.log(LogCategory.Config, LogLevel.Warn, "Error while loading project from user settings. Opening dialog instead...");
          openOpenProjectDialog();
        }
      }
    }

    public override void Terminate() {
    }

    public override void Update() {
      ecs.Update();

      if (ui?.SideComboBox?.SelectedItem != null) {
        ChangeSide(ui.SideComboBox.SelectedItem.Text);
      }

      if(ui != null)
        updateIncrement();
    }

    public override void Draw() {
      ecs.Draw();
      desktop?.Render();
    }

    public void RefreshEntity() {
      if (entity != null) {
        entity.Destroy();
      }
      entity = EntityFactory.CreateEntity(new Vector2(0, 100));
      spineComponent = entity.GetComponent<SpineComponent>();
      drawComponent = entity.GetComponent<DrawComponent>();
    }

    public void EnableSkin(string skinName) {
      spineComponent?.AddSkin(skinName);
    }

    public void DisableSkin(string skinName) {
      spineComponent?.RemoveSkin(skinName);
    }

    public void ChangeSide(string currentSide) {
      foreach (var availableSide in Project.ProjectConfig.AvailableSidePrefixes.Split(",")) {
        if (availableSide.Equals(currentSide)) {
          spineComponent?.SetSlotsVisibleByPrefix(availableSide, true);
        } else {
          spineComponent?.SetSlotsVisibleByPrefix(availableSide, false);
        }
      }
    }

    public void PlayAnimation(string animationName) {
      spineComponent.PlayAnimation(0, animationName, true);
    }

    public void UpdateColors(string suffix, Color color) {
      spineComponent.ApplyColorBySuffix(suffix, color);
    }

    public void SetEntityVisibility(bool value) {
      if (drawComponent != null)
        drawComponent.IsVisible = value;
    }

    private void openOpenProjectDialog() {
      var openProjectDialog = new FileDialog(FileDialogMode.ChooseFolder);
      openProjectDialog.Title = "Select folder with MonoPanda project or game";
      openProjectDialog.ShowModal(desktop);

      openProjectDialog.Closed += (sender, args) => {
        var path = openProjectDialog.FilePath.Replace("\\", "/");

        if (openProjectDialog.Result) {
          openProject(path);
        } else {
          openOpenProjectDialog();
        }
      };
    }

    private void openProject(string path) {
      if (!path.EndsWith("/")) {
        path += "/";
      }
      Project.OpenProject(path);
      ui = new UI(this);
      desktop.Widgets.Clear();
      desktop.Widgets.Add(ui);
      if (!string.IsNullOrEmpty(Settings.SkinManager.StartSpineProjectId)) {
        openSpineProject(Settings.SkinManager.StartSpineProjectId);
      }
      RefreshEntity();
      if (!string.IsNullOrEmpty(Settings.SkinManager.EnableSkins)) {
        foreach (var skinName in Settings.SkinManager.EnableSkins.Split(",")) {
          ui.EnableCoreSkin(skinName);
        }
      }
    }

    private void openSpineProject(string id) {
      foreach (var item in ui.SpineAssetsComboBox.Items) {
        if (item.Text.Equals(id)) {
          ui.SpineAssetsComboBox.SelectedItem = item;
        }
      }
    }

    private void updateIncrement() {
      var increment = InputManager.GetKeyInput("increment_change").IsHeld ? 1f : 0.1f;
      ui.XSpinButton.Increment = increment;
      ui.YSpinButton.Increment = increment;
      ui.ScaleXSpinButton.Increment = increment;
      ui.ScaleYSpinButton.Increment = increment;
    }
  }
}