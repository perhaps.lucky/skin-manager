﻿namespace SkinManager {
  public class Consts {
    public const string ContentId = "content-id";
    public const string EntityFactoryId = "entity";
    public const string MonoPandaConfigPath = "Content/MonoPanda/panda-config.ini";
    public const string ModsSection = "Mods";
    public const string ModsFolder = "ModsFolder";
    public const string CoreModFolderName = "CoreModFolderName";
    public const string Content = "Asset";
    public const string ContentItemsJsonPath = "AssetsJsonPath";
    public const string ModsInfoJsonPath = "ModInfoJson";
  }
}