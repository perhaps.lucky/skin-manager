﻿using IniParser.Model;

using MonoPanda.UtilityClasses;

using System.Dynamic;

namespace MonoPanda.UserSettings {
  public class SkinManagerSettings : SettingsIniSectionLoader {

  public string StartProjectPath { get; set; }
  public string StartSpineProjectId { get; set; }
  public string EnableSkins { get; set; }

  public string AssetsPath { get; set; }

  public SkinManagerSettings(IniData iniData) : base(iniData, "SkinManager") { }
  }
}