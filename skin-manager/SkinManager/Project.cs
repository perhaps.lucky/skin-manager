﻿using IniParser.Exceptions;
using IniParser.Model;

using MonoPanda;
using MonoPanda.Configuration;
using MonoPanda.Content;
using MonoPanda.Logger;
using MonoPanda.Spine.MonoPanda;
using MonoPanda.Utils;

using Newtonsoft.Json;

using Spine;

using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace SkinManager {
  public class Project {
    private static Project instance;

    private string projectPath;
    private string modsPath;
    private string coreMod;
    private string activeMod;
    private string contentItemsJsonPath;
    private string activeSpineProject;
    private ContentItem activeSpineAsset;
    private List<ContentItem> contentItems;
    private List<ExternalSkin> workingSkins;
    private SkinManagerProjectConfig skinManagerProjectConfig;
    private IniData activeProjectConfig;
    private string modsInfoJsonPath;

    public static SkinManagerProjectConfig ProjectConfig => instance.skinManagerProjectConfig;

    public static string ModsPath => instance.modsPath;
    public static string CoreMod => instance.coreMod;

    public static string ActiveMod => instance.activeMod;

    public static string GetActiveModPath() {
      return GetActiveProjectPath() + ModsPath + ActiveMod; // TODO: active mod stuff
    }

    public static void OpenProject(string projectPath) {
      instance = new Project();
      instance.openProject(projectPath);
      GameMain.UpdateTitleBar();
    }

    public static void OpenSpineProject(string contentId) {
      instance.setActiveSpineProject(contentId);
    }

    public static string GetActiveSpineProject() {
      return instance.activeSpineProject;
    }

    public static List<ContentItem> GetSpineAssets() {
      return instance.contentItems.FindAll(item => item.Type == "SpineAsset");
    }

    public static List<ContentItem> GetContentItems() {
      return instance.contentItems;
    }

    public static List<ExternalSkin> GetWorkingExternalSkins() {
      if (instance.workingSkins != null) {
        return instance.workingSkins;
      } else return new List<ExternalSkin>();
    }

    public static void Save() {
      instance.save();
    }

    public static void SaveProjectConfig() {
      instance.saveProjectConfig();
    }

    public static ExposedList<Animation> GetAnimations() {
      if (instance.activeSpineProject == null) {
        return new ExposedList<Animation>();
      }

      return ContentManager.Get<SpineAsset>(instance.activeSpineProject, true).SkeletonData.Animations;
    }

    public static string GetActiveProjectPath() {
      return instance.projectPath;
    }

    public static void RemoveContentItem(string id) {
      instance.removeContentItem(id);
    }

    public static List<Skin> GetCoreSkins() {
      if (instance.activeSpineProject == null) {
        return new List<Skin>();
      }

      List<Skin> skins =
        new List<Skin>(ContentManager.Get<SpineAsset>(instance.activeSpineProject, true).SkeletonData.Skins);
      skins.RemoveAll(skin =>
        GetWorkingExternalSkins().Find(externalSkin => externalSkin.Name.Equals(skin.Name)) != null);
      return skins;
    }

    public static void RefreshSkinsInContentManager() {
      instance.refreshSkinsInContentManager();
    }

    public static void SaveContentItems() {
      instance.saveContentItems();
    }

    public static void AddNewContentItem(ContentItem contentItem) {
      GetContentItems().Add(contentItem);
      ContentManager.LoadExternalItem(contentItem, GetActiveModPath());
    }

    public static List<string> GetMods() {
      return instance.getMods();
    }

    public static void SetActiveMod(string modName) {
      instance.setActiveMod(modName);
    }

    private void openProject(string projectPath) {
      ContentManager.unload();
      this.projectPath = projectPath;
      loadSkinsManager();
      loadMonoPandaConfig();
      activeMod = coreMod;
      loadContentItems();
    }

    private void loadSkinsManager() {
      try {
        IniData iniData = IniUtils.getIniData(projectPath + Config.SkinManager.ProjectConfigFile);
        skinManagerProjectConfig = new SkinManagerProjectConfig(iniData);
      } catch (ParsingException e) {
        Log.log(LogCategory.Config, LogLevel.Warn, $"No skin manager config for project: {projectPath}");
        skinManagerProjectConfig = new SkinManagerProjectConfig();
      }
    }

    private void setActiveSpineProject(string contentId) {
      ContentManager.unload();
      activeSpineProject = contentId;
      activeSpineAsset = GetSpineAssets().Find(asset => asset.Id.Equals(contentId));
      loadSpineProject(activeSpineAsset);
    }

    private void loadMonoPandaConfig() {
      activeProjectConfig = IniUtils.getIniData(projectPath + Consts.MonoPandaConfigPath);
      modsPath = activeProjectConfig[Consts.ModsSection][Consts.ModsFolder];
      coreMod = activeProjectConfig[Consts.ModsSection][Consts.CoreModFolderName];
      contentItemsJsonPath = activeProjectConfig[Consts.Content][Consts.ContentItemsJsonPath];
      modsInfoJsonPath = activeProjectConfig[Consts.ModsSection][Consts.ModsInfoJsonPath];
    }

    private void loadContentItems() {
      if (File.Exists(getModPath(activeMod) + "/" + contentItemsJsonPath))
        contentItems = ContentUtils.LoadJson<List<ContentItem>>(getModPath(activeMod) + "/" + contentItemsJsonPath);
      else {
        contentItems = new List<ContentItem>();
      }
    }

    private void removeContentItem(string id) {
      var contentItem = contentItems.Find(c => c.Id.Equals(id));
      if (contentItem != null) {
        var pathPrefix = $"{projectPath}{modsPath}{activeMod}/";
        switch (contentItem.Type) {
          case "Song":
          case "SoundEffect":
          case "SpriteAsset":
          case "TiledMap":
            File.Delete($"{pathPrefix}{contentItem.FilePath}");
            break;
          case "SpineAsset":
            File.Delete($"{pathPrefix}{contentItem.AtlasPath}");
            File.Delete($"{pathPrefix}{contentItem.JsonPath}");
            File.Delete($"{pathPrefix}{contentItem.AtlasPath.Replace(".atlas", ".png")}");
            break;
          case "SpriteSheet":
            File.Delete($"{pathPrefix}{contentItem.TexturePath}");
            File.Delete($"{pathPrefix}{contentItem.JsonPath}");
            break;
        }

        contentItems.Remove(contentItem);
        saveContentItems();
      }
    }

    private void loadSpineProject(ContentItem spineAsset) {
      var relPath = spineAsset.AtlasPath;
      ContentManager.LoadExternalItem(activeSpineAsset, getModPath(coreMod));
      loadExternalSkinsFromActiveMod(relPath);
    }

    private void refreshSkinsInContentManager() {
      ContentManager.ReloadActiveSpineProject();
    }

    private void save() {
      ContentManager.SaveActiveSpineProject();
    }

    private void loadExternalSkinsFromActiveMod(string relPath) {
      string externalSkinsPath = getModPath(activeMod) + "/" + relPath.Replace(".atlas", "_skins.json");
      if (!File.Exists(externalSkinsPath)) {
        workingSkins = new List<ExternalSkin>();
        return;
      }

      workingSkins =
        ContentUtils.LoadJson<List<ExternalSkin>>(externalSkinsPath);
      foreach (var externalSkin in workingSkins) {
        foreach (var attachment in externalSkin.Attachments) {
          var contentId = attachment.ContentId;
          ContentItem contentItem = contentItems.Find((item) => item.Id.Equals(contentId));
          if (contentItem != null && ContentManager.getItem(contentItem.Id) == null) {
            ContentManager.LoadExternalItem(contentItem, getModPath(activeMod));
          }
        }
      }
    }

    private void saveContentItems() {
      File.WriteAllText(getModPath(activeMod) + "/" + contentItemsJsonPath,
        JsonConvert.SerializeObject(contentItems, Formatting.Indented));
    }

    private string getModPath(string modName) {
      return projectPath + modsPath + modName;
    }

    private void saveProjectConfig() {
      IniData iniData = new IniData();

      iniData["SkinManager"]["AvailableSidePrefixes"] = ProjectConfig.AvailableSidePrefixes;
      iniData["SkinManager"]["AvailableColorChannelSuffixes"] = ProjectConfig.AvailableColorChannelSuffixes;
      iniData["SkinManager"]["ImportedSkinDefaultLocation"] = ProjectConfig.ImportedSkinDefaultLocation;
      iniData["SkinManager"]["AutoMeshMap"] = ProjectConfig.AutoMeshMap;
      iniData["SkinManager"]["MultiSlotMap"] = ProjectConfig.MultiSlotMap;

      IniUtils.saveIniData(iniData, projectPath + Config.SkinManager.ProjectConfigFile);
    }

    private List<string> getMods() {
      List<string> mods = new List<string>();
      foreach (string folderName in Directory.EnumerateDirectories($"{GetActiveProjectPath()}{ModsPath}")
        .Select(Path.GetFileName)) {
        if (File.Exists($"{GetActiveProjectPath()}{ModsPath}{folderName}/{modsInfoJsonPath}")) {
          mods.Add(folderName);
        }
      }

      mods.Add(CoreMod);

      return mods;
    }

    private void setActiveMod(string modName) {
      activeMod = modName;
      //setActiveSpineProject(GetActiveSpineProject());
      loadContentItems();
      loadSpineProject(ContentManager.getItem(GetActiveSpineProject()));
    }
  }
}