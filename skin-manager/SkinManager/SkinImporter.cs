﻿using MonoPanda.Content;
using MonoPanda.Logger;
using MonoPanda.Spine.MonoPanda;
using MonoPanda.SpriteSheets;

using Newtonsoft.Json;

using Spine;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;

namespace SkinManager {
  public class SkinImporter {
    private string importFilePath;
    private string exportFolderPath;
    private string id;

    private SkinImporter(string importPath, string exportPath) {
      this.importFilePath = importPath;
      this.exportFolderPath = exportPath;
      this.id = Path.GetFileName(importPath).Split(".")[0];
    }


    public static bool ImportFile(string importPath, string exportPath) {
      if (!File.Exists(importPath)) {
        Log.log(LogCategory.SkinImporter, LogLevel.Error, $"File {importPath} not found!");
        return false;
      }

      if (!checkExportPath(exportPath)) {
        Log.log(LogCategory.SkinImporter, LogLevel.Error, $"Path: {exportPath} is not within mod folder");
        return false;
      }

      try {
        SkinImporter skinImporter = new SkinImporter(importPath, exportPath);
        skinImporter.performImport();
      } catch (SkinImporterException e) {
        Log.log(LogCategory.SkinImporter, LogLevel.Error, $"Skin importer failed. Reason: {e.Reason}");
        return false;
      } catch (Exception e) {
        Log.log(LogCategory.SkinImporter, LogLevel.Error, $"Skin importer failed. Unknown exception: {e}");
        return false;
      }

      Log.log(LogCategory.SkinImporter, LogLevel.Info, $"Skin import successful");
      return true;
    }

    private void performImport() {
      if (importFilePath.EndsWith(".json")) {
        importJson();
      } else if (importFilePath.EndsWith(".xcf")) {
        importGimpProject();
      } else {
        throw new SkinImporterException("Wrong file format. Available file formats are: .json and .xcf");
      }
    }

    private void importJson() {
      copySpriteSheetFiles();
      addContentItem();
      createExternalSkin();
      Project.Save();
    }

    private void copySpriteSheetFiles() {
      File.Copy(importFilePath, $"{exportFolderPath}/{id}.json", true);
      File.Copy(importFilePath.Replace(".json", ".png"), $"{exportFolderPath}/{id}.png", true);
      Log.log(LogCategory.SkinImporter, LogLevel.Info, "Copied json project files");
    }

    private void addContentItem() {
      removeContentItemIfExists();

      var relFolderPath = exportFolderPath.Replace("\\", "/").Replace($"{Project.GetActiveModPath()}/", "")
        .Replace(Project.GetActiveModPath(), "");

      Log.log(LogCategory.SkinImporter, LogLevel.Info,
        $"Adding content item: id={id} FilePath={relFolderPath}/{id}.png");
      Project.AddNewContentItem(new ContentItem {
        Id = id, TexturePath = $"{relFolderPath}/{id}.png", JsonPath = $"{relFolderPath}/{id}.json", Type = "SpriteSheet"
      });

      Project.SaveContentItems();
      Log.log(LogCategory.SkinImporter, LogLevel.Info, "List of content items saved successfully");
    }

    private void removeContentItemIfExists() {
      var contentItem = Project.GetContentItems().Find(item => item.Id.Equals(id));
      if (contentItem != null) {
        Log.log(LogCategory.SkinImporter, LogLevel.Info,
          "Content item with same id already exists and will be overrided!");
        Project.GetContentItems().Remove(contentItem);
      }
    }

    private void createExternalSkin() {
      var spriteSheet = loadSpriteSheet();
      removeSkinIfExists();
      var externalSkin = createExternalSkinFromSpriteSheet(spriteSheet);
      Project.GetWorkingExternalSkins().Add(externalSkin);
      Log.log(LogCategory.SkinImporter, LogLevel.Info, "Added new external skin");
      Project.RefreshSkinsInContentManager();
    }

    private void removeSkinIfExists() {
      var existingExternalSkin = Project.GetWorkingExternalSkins().Find(i => i.Name.Equals(id));
      if (existingExternalSkin != null) {
        Log.log(LogCategory.SkinImporter, LogLevel.Info,
          "External skin with same name already exist and will be overrided!");
        Project.GetWorkingExternalSkins().Remove(existingExternalSkin);
      }
    }

    private ExternalSkin createExternalSkinFromSpriteSheet(SpriteSheetDescription spriteSheet) {
      ExternalSkin externalSkin = new ExternalSkin();
      externalSkin.Name = id;

      foreach (var frame in spriteSheet.frames) {
        externalSkin.Attachments.AddRange(createExternalAttachments(frame, externalSkin));
      }

      return externalSkin;
    }

    private List<ExternalAttachment> createExternalAttachments(SpriteSheetFrame frame, ExternalSkin externalSkin) {
      var externalAttachments = new List<ExternalAttachment>();
      foreach (var keyValuePair in Project.ProjectConfig.MultiSlotMapDictionary) {
        if (frame.filename.Contains(keyValuePair.Key)) {
          foreach (var slotName in keyValuePair.Value) {
            externalAttachments.Add(createExternalAttachment(frame, externalSkin, slotName));
          }

          return externalAttachments;
        }
      }

      externalAttachments.Add(createExternalAttachment(frame, externalSkin));
      return externalAttachments;
    }

    private ExternalAttachment createExternalAttachment(SpriteSheetFrame frame, ExternalSkin externalSkin,
      string slotName = null) {
      ExternalAttachment externalAttachment = new ExternalAttachment();
      externalAttachment.SlotName = slotName != null ? slotName : frame.filename;
      externalAttachment.ContentId = id;
      externalAttachment.Height = frame.sourceSize.h;
      externalAttachment.Width = frame.sourceSize.w;
      externalAttachment.AtlasFileName = frame.filename;
      checkForAutoMesh(frame, externalAttachment, externalSkin);
      return externalAttachment;
    }

    private void checkForAutoMesh(SpriteSheetFrame frame, ExternalAttachment externalAttachment,
      ExternalSkin externalSkin) {
      checkForAutoMeshInDictionary(frame, externalAttachment);
      checkForAutoMeshInSpineProject(externalAttachment, externalSkin);
    }

    private void checkForAutoMeshInDictionary(SpriteSheetFrame frame, ExternalAttachment externalAttachment) {
      foreach (var keyValuePair in Project.ProjectConfig.AutoMeshMapDictionary) {
        if (frame.filename.Contains(keyValuePair.Key)) {
          externalAttachment.MeshSkinName = keyValuePair.Value;
          return;
        }
      }
    }

    private void checkForAutoMeshInSpineProject(ExternalAttachment externalAttachment, ExternalSkin externalSkin) {
      var searchFor = $"{externalSkin.Name}_mesh";
      foreach (var skin in Project.GetCoreSkins()) {
        if (skin.Name.Equals(searchFor)) {
          int slotIndex = ContentManager.Get<SpineAsset>(Project.GetActiveSpineProject(), true).SkeletonData
            .FindSlotIndex(externalAttachment.SlotName);
          var tryFindMesh = skin.GetAttachment(slotIndex, externalAttachment.SlotName);
          if (tryFindMesh != null && tryFindMesh is MeshAttachment) {
            externalAttachment.MeshSkinName = skin.Name;
          }

          return;
        }
      }
    }

    private SpriteSheetDescription loadSpriteSheet() {
      return JsonConvert.DeserializeObject<SpriteSheetDescription>(
        File.ReadAllText(importFilePath));
    }

    private void importGimpProject() {
      Process exportProcess = new Process();
      var folderPath = Path.GetDirectoryName(importFilePath);
      exportProcess.StartInfo.FileName = $"{folderPath}/export.bat";
      exportProcess.StartInfo.WorkingDirectory = folderPath;
      exportProcess.Start();
      Log.log(LogCategory.SkinImporter, LogLevel.Info, "Started external export process");
      exportProcess.WaitForExit();

      var newFileToImport = $"{folderPath}\\exported\\{id}.json";
      Log.log(LogCategory.SkinImporter, LogLevel.Info, $"External process finished. Importing file: {newFileToImport}");
      SkinImporter skinImporter = new SkinImporter(newFileToImport, exportFolderPath);
      skinImporter.performImport();
    }


    private static bool checkExportPath(string exportPath) {
      return Utils.IsDirectoryWithinAnother(exportPath, Project.GetActiveModPath());
    }
  }

  public class SkinImporterException : Exception {
    public string Reason { get; }

    public SkinImporterException(string reason) {
      Reason = reason;
    }
  }
}