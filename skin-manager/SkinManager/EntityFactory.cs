﻿using Microsoft.Xna.Framework;

using MonoPanda.Components;
using MonoPanda.ECS;
using MonoPanda.ECS.Components.DrawingComponent;

namespace SkinManager {
  public class EntityFactory : MonoPanda.ECS.EntityFactory {

    private static EntityFactory instance;
    
    protected override void SetAsInstance() {
      instance = this;
    }

    public static Entity CreateEntity(Vector2 position) {
      instance.Models[0].Components["MonoPanda.Components.DrawComponent"]["ContentId"] = Project.GetActiveSpineProject();
      return instance.CreateEntityInner(Consts.EntityFactoryId, position);
    }
  }
}