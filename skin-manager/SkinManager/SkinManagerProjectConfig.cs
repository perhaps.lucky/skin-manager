﻿using IniParser.Model;

using MonoPanda.UtilityClasses;

using System;
using System.Collections.Generic;

namespace SkinManager {
  // External config for project
  public class SkinManagerProjectConfig : DefaultIniSectionLoader {
    public string AvailableSidePrefixes { get; set; }
    public string AvailableColorChannelSuffixes { get; set; }
    public string ImportedSkinDefaultLocation { get; set; }
    public string AutoMeshMap { get; set; }

    public string MultiSlotMap { get; set; }
    public Dictionary<string, string> AutoMeshMapDictionary { get; private set; }

    public Dictionary<string, List<string>> MultiSlotMapDictionary { get; private set; }

    public SkinManagerProjectConfig(IniData iniData) : base(iniData, "SkinManager") {
      MultiSlotMapDictionary = new Dictionary<string, List<string>>();
      AutoMeshMapDictionary = new Dictionary<string, string>();
      RefreshAutoMeshMapDictionary();
      RefreshMultiSlotMapDictionary();
    }

    public SkinManagerProjectConfig() : base(new IniData(), "SkinManager") {
      AvailableSidePrefixes = "";
      AvailableColorChannelSuffixes = "";
      ImportedSkinDefaultLocation = "";
      AutoMeshMap = "";
      MultiSlotMap = "";
      RefreshAutoMeshMapDictionary();
      RefreshMultiSlotMapDictionary();
    }

    public void RefreshAutoMeshMapDictionary() {
      if (AutoMeshMap == null) return;

      AutoMeshMapDictionary = new Dictionary<string, string>();
      foreach (var pair in AutoMeshMap.Split(";")) {
        var keyValuePair = pair.Split(":");
        if (keyValuePair.Length == 2) {
          AutoMeshMapDictionary.Add(keyValuePair[0], keyValuePair[1]);
        }
      }
    }

    public void RefreshMultiSlotMapDictionary() {
      if (MultiSlotMap == null) return;

      MultiSlotMapDictionary = new Dictionary<string, List<string>>();
      foreach (var record in MultiSlotMap.Split(";")) {
        var keyValuePair = record.Split(":");
        if (keyValuePair.Length == 2) {
          MultiSlotMapDictionary.Add(keyValuePair[0], new List<string>(keyValuePair[1].Split(",")));
        }
      }
    }
  }
}