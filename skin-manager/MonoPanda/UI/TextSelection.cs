﻿using Microsoft.Xna.Framework;
using MonoPanda.BitmapFonts;
using System;
using System.Collections.Generic;

namespace MonoPanda.UI {
  public class TextSelection : UIObject, IForceOnChange {

    public Action<ISelectionItem> OnChange { get; set; }
    public string FontId { get => fontId; set => setFont(value); }
    public Vector2 Position { get => position; set => setPosition(value); }
    public Color Color { get => color; set => setColor(value); }
    public int FontSize { get => fontSize; set => setFontSize(value); }
    public Color HoverColor { set => setHoverColor(value); }
    public Color ClickColor { set => setClickColor(value); }
    public TextButton LeftButton { get; private set; }
    public TextButton RightButton { get; private set; }
    public Text DescriptionText { get; private set; }
    public List<ISelectionItem> Items { get => items; }

    private string fontId;
    private Vector2 position;
    private Color color;
    private int fontSize;

    private float distanceToText;

    private int currentItemIndex;
    private int maxItemIndex;
    private List<ISelectionItem> items;

    public TextSelection(string fontId, Vector2 position, float distanceToText, float spaceForText, string description, Color defaultColor, Color hoverColor, Color clickColor, List<ISelectionItem> items, int fontSize = 32, Action<ISelectionItem> onChangeAction = null) {
      this.fontId = fontId;
      this.position = position;
      this.color = defaultColor;
      this.fontSize = fontSize;
      this.OnChange = onChangeAction;

      this.DescriptionText = new Text(fontId, position, description, defaultColor, fontSize);
      this.LeftButton = new TextButton(fontId, position + new Vector2(0, fontSize * 1.5f), "<", fontSize, onLeft, defaultColor, hoverColor, clickColor);
      this.RightButton = new TextButton(fontId, position + new Vector2(2 * distanceToText + spaceForText, fontSize * 1.5f), ">", fontSize, onRight, defaultColor, hoverColor, clickColor);

      this.items = items;
      this.maxItemIndex = items.Count-1;
      this.distanceToText = distanceToText;
    }

    public ISelectionItem GetActiveItem() {
      return items[currentItemIndex];
    }

    public override void Draw() {
      DescriptionText.Draw();
      LeftButton.Draw();
      RightButton.Draw();
      FontRenderer.DrawText(FontId, LeftButton.Position + new Vector2(distanceToText, 0), GetActiveItem().GetLabel(), Color, FontSize);
    }

    public override void Update() {
      LeftButton.Update();
      RightButton.Update();
    }

    private void onLeft() {
      if (currentItemIndex == 0)
        currentItemIndex = maxItemIndex;
      else
        currentItemIndex--;
      
      if(OnChange != null)
        OnChange.Invoke(GetActiveItem());
    }

    private void onRight() {
      if (currentItemIndex == maxItemIndex)
        currentItemIndex = 0;
      else
        currentItemIndex++;

      if(OnChange != null)
        OnChange.Invoke(GetActiveItem());
    }

    private void setFont(string value) {
      fontId = value;
      LeftButton.FontId = value;
      RightButton.FontId = value;
      DescriptionText.FontId = value;
    }

    private void setPosition(Vector2 value) {
      position = value;
      LeftButton.Position = value;
      RightButton.Position = value;
      DescriptionText.Position = value;
    }

    private void setColor(Color value) {
      color = value;
      LeftButton.DefaultColor = value;
      RightButton.DefaultColor = value;
      DescriptionText.Color = value;
    }

    private void setHoverColor(Color value) {
      LeftButton.HoverColor = value;
      RightButton.HoverColor = value;
    }

    private void setClickColor(Color value) {
      LeftButton.ClickColor = value;
      RightButton.ClickColor = value;
    }

    private void setFontSize(int value) {
      fontSize = value;
      LeftButton.FontSize = value;
      RightButton.FontSize = value;
      DescriptionText.FontSize = value;
    }

    public void ForceOnChange() {
      OnChange.Invoke(GetActiveItem());
    }
  }
}
