﻿using Microsoft.Xna.Framework;
using MonoPanda.BitmapFonts;
using MonoPanda.Configuration;
using MonoPanda.Input;
using MonoPanda.UserSettings;
using MonoPanda.Utils;
using System;

namespace MonoPanda.UI {
  public class TextButton : UIObject {

    public Color DefaultColor { get; set; }
    public Color HoverColor { get; set; }
    public Color ClickColor { get; set; }
    public Vector2 Position { get; set; }
    public string Text { get; set; }
    public string FontId { get; set; }
    public int FontSize { get; set; }
    public Action Action { get; set; }

    private Vector2 endCorner;

    private bool isHover;
    private bool isClicked;

    public TextButton(string fontId, Vector2 position, string text, int fontSize, Action action, Color defaultColor, Color hoverColor, Color clickColor) {
      Text = text;
      Position = position;
      FontId = fontId;
      FontSize = fontSize;
      Action = action;
      DefaultColor = defaultColor;
      HoverColor = hoverColor;
      ClickColor = clickColor;
    }

    public TextButton(string fontId, Vector2 position, string text, int fontSize, Action action, Color defaultColor, Color hoverColor) : this(fontId, position, text, fontSize, action, defaultColor, hoverColor, defaultColor) { }

    public TextButton(string fontId, Vector2 position, string text, int fontSize, Action action, Color defaultColor) : this(fontId, position, text, fontSize, action, defaultColor, defaultColor) { }

    public override void Update() {
      if (endCorner != null) {
        if (MouseUtils.IsCursorInRectangleInWindow(getWindowScaledPosition(Position), endCorner)) {
          isHover = true;
          if (InputManager.GetLeftMouseButton().IsPressed) {
            isClicked = true;
            Action.Invoke();
          } else
            isClicked = false;
        } else
          isHover = false;
      }
    }

    public override void Draw() {
      endCorner = FontRenderer.DrawText(FontId, Position, Text, isHover ? (isClicked ? ClickColor : HoverColor) : DefaultColor, FontSize);
    }

    private Vector2 getWindowScaledPosition(Vector2 position) {
      var scale = (float)Settings.Window.Height / Config.Window.Height;
      return new Vector2(position.X * scale, position.Y * scale); 
    }
  }
}
