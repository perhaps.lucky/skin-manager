﻿using Microsoft.Xna.Framework;
using MonoPanda.BitmapFonts;

namespace MonoPanda.UI {
  public class Text : UIObject {

    public string FontId { get; set; }
    public Vector2 Position { get; set; }
    public string Txt { get; set; }
    public Color Color { get; set; }
    public int FontSize { get; set; }

    public Text(string fontId, Vector2 position, string txt, Color color, int fontSize = 32) {
      this.FontId = fontId;
      this.Position = position;
      this.Txt = txt;
      this.Color = color;
      this.FontSize = fontSize;
    }

    public override void Draw() {
      FontRenderer.DrawText(FontId, Position, Txt, Color, FontSize);
    }

    public override void Update() {
      // should be empty, doesn't need update
    }
  }
}
