﻿using MonoPanda.Tiled.Json;

using System;

namespace MonoPanda.Tiled.Handlers {
  public class TiledLayerHandler {
    private Action<TiledLayer> create;
    private Action<TiledLayer> postInit;

    public TiledLayerHandler(Action<TiledLayer> create = null, Action<TiledLayer> postInit = null) {
      this.create = create;
      this.postInit = postInit;
    }

    public void HandleCreate(TiledLayer layer) {
      if (create != null)
        create.Invoke(layer);
    }

    public void HandlePostInit(TiledLayer layer) {
      if (postInit != null)
        postInit.Invoke(layer);
    }
  }
}