﻿using System.Collections.Generic;

namespace MonoPanda.Tiled.Json {
  public class TiledObject : TiledClassWithProperties {
    public TiledMap Map { get; set; }
    public float Height { get; set; }
    public float Width { get; set; }
    public int Id { get; set; }
    public float Rotation { get; set; }
    public string Type { get; set; }
    public bool Visible { get; set; }
    public float X { get; set; }
    public float Y { get; set; }
    public TiledText Text { get; set; }
    public int GID { get; set; }
    public string Name { get; set; }
    public List<TiledProperty> Properties { get; set; }
    protected override List<TiledProperty> getPropertyList() {
      return Properties;
    }

    protected override TiledMap getMap() {
      return Map;
    }

    public TiledTile GetTileFromTileset() {
      if (GID == 0)
        return null;

      foreach (var tileSet in Map.Tilesets) {
        if(tileSet.FirstGid > GID)
          continue;
        return tileSet.FindTileByGID(GID);
      }

      return null;
    }
  }
}