﻿using Microsoft.Xna.Framework;

using MonoPanda.ECS;
using MonoPanda.Utils;

using Newtonsoft.Json;

using Spine;

using System.Collections.Generic;
using System.ComponentModel;
using System.Dynamic;

namespace MonoPanda.Tiled.Json {
  public class TiledLayer : TiledClassWithProperties {

    public TiledMap Map { get; set; }

    public Dictionary<int, Entity> IndexToCreatedEntity { get; set; } = new Dictionary<int, Entity>();
    public int[] Data { get; set; }

    public float Height { get; set; }
    
    public float Width { get; set; }

    public int Id { get; set; }

    public string Name { get; set; }

    public TiledLayerType Type { get; set; }

    public bool Visible { get; set; }
    
    public string TintColor { get; set; } = "#ffffff";

    public string Color { get; set; } = "#ffffff";

    public float OffsetX { get; set; }

    public float OffsetY { get; set; }

    public float Opacity { get; set; }

    public List<TiledObject> Objects { get; set; } = new List<TiledObject>();

    public List<TiledProperty> Properties { get; set; } = new List<TiledProperty>();
    
    public int LayerIndex { get; set; }

    public Vector2 GetOffset() {
      return new Vector2(OffsetX, OffsetY);
    }

    public Color GetTintColor() {
      return ColorUtils.HexStringToColor(TintColor);
    }

    protected override List<TiledProperty> getPropertyList() {
      return Properties;
    }

    protected override TiledMap getMap() {
      return Map;
    }

    public Color GetColor() {
      return ColorUtils.HexStringToColor(Color);
    }
  }
}