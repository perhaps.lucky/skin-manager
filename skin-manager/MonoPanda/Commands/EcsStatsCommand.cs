﻿using MonoPanda.ECS;
using System.Collections.Generic;

namespace MonoPanda.Commands {
  public class EcsStatsCommand : Command {

    public EcsStatsCommand() {
      HelpCategoryName = HelpCategories.World;
      ShortHelpDescription = "Prints numbers of entities, systems, components in the entire ECS";
      ExtendedHelp = "ecsstats \n\nPrints numbers of entities, systems, components in the entire ECS";
    }


    public override string Execute(List<string> parameters) {
      return Optional<EntityComponentSystem>.Of(ECS).IfPresentGet(ecs => ecs.ToString()).OrElse("Console system doesn't have assigned ECS");
    }
  }
}
