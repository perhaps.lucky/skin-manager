﻿using MonoPanda.ECS;

using System.Collections.Generic;

namespace MonoPanda.Commands {
  public class InspectEntityCommand : Command {

    public InspectEntityCommand() {
      HelpCategoryName = HelpCategories.World;
      ShortHelpDescription = "Prints informations about entity";
      ExtendedHelp = "inspectentity %{LightSkyBlue}% [entity_id] \n\n"
        + "%{LightSkyBlue}% entity_id %{White}% - id of entity to inspect";
    }


    public override string Execute(List<string> parameters) {
      var entityName = parameters[0];

      return ECS.GetEntityOptional(entityName)
                .IfPresentGet(entity => entity.ToString())
                .OrElse("Entity: " + entityName + " not found.");
    }
  }
}
