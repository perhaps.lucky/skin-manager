﻿using System.Collections.Generic;

namespace MonoPanda.Commands {
  public class HelpCommand : Command {

    public HelpCommand() {
      ShowInHelp = false;
      ExtendedHelp = "%{150,189,204}% Wait. That's illegal.";
    }

    public override string Execute(List<string> parameters) {
      if (parameters.Count == 0) {
        return listOfCommands();
      } else {
        return descriptionOfCommand(parameters[0]);
      }
    }

    private string listOfCommands() {
      string response = " Available commands:";
      Dictionary<string, List<string>> handlersByCategory = new Dictionary<string, List<string>>(); // gg
      foreach (KeyValuePair<string, Command> keyValue in CommandSystem.CommandHandlers) {
        if (keyValue.Value.ShowInHelp) {
          addToCategory(handlersByCategory, keyValue);
        }
      }

      foreach (KeyValuePair<string, List<string>> keyValue in handlersByCategory) {
        response += "\n\n %{Cyan}% -=  " + keyValue.Key + "  =-";
        foreach (string commandName in keyValue.Value) {
          response += "\n %{Yellow}% " + commandName + " %{White}% - " + CommandSystem.CommandHandlers[commandName].ShortHelpDescription;
        }
      }

      response += "\n\n Use %{Cyan}% help %{Yellow}% [command name] %{White}% for detailed help.";
      return response;
    }

    private string descriptionOfCommand(string commandName) {
      return
        CommandSystem.CommandHandlers.ContainsKey(commandName)
        ? CommandSystem.CommandHandlers[commandName].ExtendedHelp
        : commandName + " - command not found.";
    }

    private void addToCategory(Dictionary<string, List<string>> dictionary, KeyValuePair<string, Command> commandHandler) {
      var categoryName = commandHandler.Value.HelpCategoryName;
      if (!dictionary.ContainsKey(categoryName))
        dictionary.Add(categoryName, new List<string>());
      dictionary[categoryName].Add(commandHandler.Key);
    }
  }
}
