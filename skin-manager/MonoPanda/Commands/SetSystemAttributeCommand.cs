﻿using MonoPanda.ECS;
using MonoPanda.ParameterConvert;

using System;
using System.Collections.Generic;
using System.Reflection;

namespace MonoPanda.Commands {
  public class SetSystemAttributeCommand : Command {

    public SetSystemAttributeCommand() {
      HelpCategoryName = HelpCategories.World;
      ShortHelpDescription = "Change value of system attribute";
      ExtendedHelp = "setsystemattribute %{Plum}% [system_name] %{Lime}% [attribute_name] %{Yellow}% [value] \n\n "
                    + "%{Plum}% system_name %{White}% - short identificator of component (e.g. %{Yellow}% DrawSystem %{White}% ). \n"
                    + "%{Lime}% attribute_name %{White}% - name of attribute which value will be changed. \n"
                    + "%{Yellow}% value %{White}% - new value for attribute.";
    }

    public override string Execute(List<string> parameters) {
      var systemName = parameters[0];
      var propertyName = parameters[1];
      var value = parameters[2];

      EntitySystem system = ECS.GetSystemByIdentifier(systemName);
      if (system == null)
        return "System not found: " + systemName;
      Type systemType = system.GetType();

      PropertyInfo propertyInfo = systemType.GetProperty(propertyName);
      if (propertyInfo == null)
        return "Property not found: " + propertyName;

      ParameterConvertAttribute attribute = propertyInfo.GetCustomAttribute<ParameterConvertAttribute>();
      if (attribute != null) {
        propertyInfo.SetValue(system, attribute.Convert(value));
      } else {
        propertyInfo.SetValue(system, Convert.ChangeType(value, propertyInfo.PropertyType));
      }

      return "%{LimeGreen}%Set ok";
    }
  }
}
