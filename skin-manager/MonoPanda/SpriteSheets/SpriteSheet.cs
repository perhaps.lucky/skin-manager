﻿using Microsoft.Xna.Framework.Graphics;
using MonoPanda.Multithreading;
using MonoPanda.Utils;
using System;

namespace MonoPanda.SpriteSheets {
  public class SpriteSheet : IDisposable {

    public Texture2D Texture { get; private set; }
    public SpriteSheetDescription SpriteSheetDescription { get; private set; }

    public SpriteSheet(StreamHolder holder) {
      Texture = ContentUtils.LoadTexture(holder);
      SpriteSheetDescription = ContentUtils.LoadJson<SpriteSheetDescription>(holder.Path.Replace(".png", ".json"));
    }

    public void Dispose() {
      Texture.Dispose();
      SpriteSheetDescription = null;
    }
  }
}
