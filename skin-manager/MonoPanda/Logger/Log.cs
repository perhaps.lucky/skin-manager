﻿using MonoPanda.Timers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Linq;
using System.Text;
using MonoPanda.Utils;
using MonoPanda.Configuration;
using MonoPanda.Console;

namespace MonoPanda.Logger {
  public class Log {
    // these fields aren't configured in panda-config.ini because they need to be loaded before rest of the config
    private const string SETTINGS_JSON_PATH = "Content/MonoPanda/logger.json";
    private const string LOGGER_LOG_FILE = "logger.txt";
    private const string LOGGER_LOG_TIMESTAMP_FORMAT = "yyyy-MM-dd HH:mm:ss";

    private static LoggerSettings settings;
    private static bool loaded;
    private static List<string> linesToSave = new List<string>();
    private static RepeatingTimer fileSaveTimer;
    private static string logFileName;

    public static void initialize() {
      innerLog("Initializing settings");
      try {
        settings = ContentUtils.LoadJson<LoggerSettings>(SETTINGS_JSON_PATH);
      } catch (FileNotFoundException) {
        innerLog("(WARN) File: " + SETTINGS_JSON_PATH + " has not been found. Logging will not work!");
      } catch (Exception e) {
        innerLog("(WARN) Exception while loading logger settings: " + e.Message + ". Logging will not work!");
      }

      if (settings != null) {
        loaded = true;
        if (settings.FileOutputEnabled) {
          logFileName = getCurrentLogFile();
          fileSaveTimer = new RepeatingTimer(settings.FileSaveInterval, "LogSaveToFileTimer");
          fileSaveTimer.Initialize();
        }
        innerLog("Settings initialized successfully!");
      }
    }

    public static void log(LogCategory category, LogLevel level, string msg) {
      if (!loaded)
        return;

      LevelSettings levelSettings = settings.LevelSettings.Find(s => s.Category == category);
      if (levelSettings == null) {
        levelSettings = addMissingLevelSettings(category);
        settings.LevelSettings.Add(levelSettings);
      }

      if (levelSettings.Level >= level) {
        msg = addTags(msg, category, level);
        Debug.WriteLine(msg);
        queueToFile(msg);

        if (Config.Console.ConsoleActive && ConsoleSystem.GetWriteLogs()) {
          ConsoleSystem.WriteToConsole(prepareForConsole(msg, level));
        }
      }
    }

    public static void log(LogCategory category, LogLevel level, object o) {
      log(category, level, o.ToString());
    }

    public static void log<T>(LogCategory category, LogLevel level, List<T> list) {
      StringBuilder sb = new StringBuilder();
      sb.Append("[ ");
      foreach (object o in list)
        sb.Append(o.ToString() + ", ");
      sb.Remove(sb.Length - 2, 2);
      sb.Append(" ]");
      log(category, level, sb.ToString());
    }

    public static void log<T>(LogCategory category, LogLevel level, string msg, List<T> list) {
      StringBuilder sb = new StringBuilder();
      sb.Append(msg);
      sb.Append("[ ");
      foreach (object o in list)
        sb.Append(o.ToString() + ", ");
      sb.Remove(sb.Length - 2, 2);
      sb.Append(" ]");
      log(category, level, sb.ToString());
    }

    public static void update() {
      if (fileSaveTimer != null && linesToSave.Count > 0 && fileSaveTimer.Check()) {
        Thread t = new Thread(saveToFile);
        t.Start();
      }
    }

    private static void innerLog(string msg) {
      msg = DateTime.Now.ToString(LOGGER_LOG_TIMESTAMP_FORMAT) + " [ Logger ] " + msg;

      Debug.WriteLine(msg);

      using (StreamWriter writer = File.AppendText(LOGGER_LOG_FILE)) {
        writer.WriteLine(msg);
      }
    }

    private static LevelSettings addMissingLevelSettings(LogCategory category) {
      LevelSettings levelSettings = new LevelSettings();
      levelSettings.Category = category;
      innerLog("(WARN) Category: " + category.ToString() + " was not configured in logger json file. Default lowest level has been set.");
      return levelSettings;
    }

    private static string addTags(string msg, LogCategory category, LogLevel level) {
      return
        (settings.Timestamps ? DateTime.Now.ToString(settings.TimestampsFormat) + " " : "") +
        "[ " + category.ToString() + " ] " +
        (level == LogLevel.Warn ? "(WARN) " : level == LogLevel.Error ? "(ERROR) " : "") +
        msg;
    }

    private static void queueToFile(string msg) {
      lock (linesToSave) {
        linesToSave.Add(msg);
      }
    }

    private static void saveToFile() {
      List<string> lines = new List<string>();
      lock (linesToSave) {
        linesToSave.ForEach(s => lines.Add(s));
        linesToSave.Clear();
      }

      using (StreamWriter writer = File.AppendText(logFileName)) {
        foreach (string line in lines)
          writer.WriteLine(line);
      }
    }

    private static string getCurrentLogFile() {
      if (!Directory.Exists(settings.FileOutputDirectory))
        createLogsDirectory();

      if (Directory.GetFiles(settings.FileOutputDirectory, "log_*").Length >= settings.MaxLogFiles)
        deleteOldestLog();

      return settings.FileOutputDirectory + "log_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".txt";
    }

    private static void createLogsDirectory() {
      Directory.CreateDirectory(settings.FileOutputDirectory);
    }

    private static void deleteOldestLog() {
      new DirectoryInfo(settings.FileOutputDirectory).EnumerateFiles().OrderByDescending(f => f.CreationTime).Last().Delete();
    }

    private static string prepareForConsole(string text, LogLevel level) {
      switch (level) {
        case LogLevel.Info:
          return "%{LightGray}% " + text;
        case LogLevel.Debug:
          return "%{DarkGray}% " + text;
        case LogLevel.Warn:
          return "%{Orange}% " + text;
        case LogLevel.Error:
          return "%{OrangeRed}% " + text;
      }
      return text;
    }
  }
}
