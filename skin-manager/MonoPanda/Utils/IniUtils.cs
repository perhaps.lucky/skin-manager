﻿using IniParser;
using IniParser.Model;
using MonoPanda.Logger;
using System.IO;
using System.Reflection;
using System.Text;

namespace MonoPanda.Utils {
  public class IniUtils {
    public const string FAKE_INI_SECTION_NAME = "root";

    public static IniData getIniData(string path) {
      Log.log(LogCategory.Ini, LogLevel.Info, "Getting ini data from: " + path);
      return new FileIniDataParser().ReadFile(path, Encoding.UTF8);
    }

    public static void saveIniData(IniData data, string path) {
      Log.log(LogCategory.Ini, LogLevel.Info, "Saving ini data to: " + path);
      Log.log(LogCategory.Ini, LogLevel.Debug, "Data:\n " + data.ToString());
      new FileIniDataParser().WriteFile(path, data);
    }

    public static void addToIniData(IniData data, object sectionObject) {
      string sectionName = sectionObject.GetType().Name.Replace("Settings", "");

      foreach (PropertyInfo propertyInfo in sectionObject.GetType().GetProperties()) {
        object value = propertyInfo.GetValue(sectionObject);
        data[sectionName][propertyInfo.Name] = value.ToString();
      }
    }

    public static IniData getIniDataNoSections(string path) {
      Log.log(LogCategory.Ini, LogLevel.Info, "Getting ini data from: " + path);
      string fakeFileContent = "[root]\n" + File.ReadAllText(path);
      using (MemoryStream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(fakeFileContent))) {
        return new FileIniDataParser().ReadData(new StreamReader(memoryStream));
      }
    }
  }
}
