﻿using System;
using System.Reflection;


namespace MonoPanda.Utils {
  public class ReflectionUtils {
    public static T GetStaticField<T>(Type type, string fieldName) {
      FieldInfo info = type.GetField(fieldName, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static);
      return (T)info.GetValue(null);
    }

    public static T GetStaticProperty<T>(Type type, string propertyName) {
      PropertyInfo info = type.GetProperty(propertyName, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static);
      return (T)info.GetValue(null);
    }

    /// <summary>
    /// <b><u>Requires class T to have a private static field of type T named instance (singleton).</u></b>
    /// </summary>
    public static T GetInstanceOf<T>() {
      return GetStaticField<T>(typeof(T), "instance");
    }
  }
}
