﻿using System.Collections.Generic;

namespace MonoPanda.Pathfinding {
  public class SearchInstance {
    public SearchParams SearchParams { get; set; }
    public PathfindingSystem PathfindingSystem { get; set; }
    private Heuristic heuristics;

    public bool FoundPath { get; private set; }
    public List<int> Path { get; private set; }

    public SearchInstance(Node from, Node to, PathfindingSystem pathfindingSystem, Heuristic heuristics) {
      SearchParams = new SearchParams(from.ID, to.ID);
      Path = new List<int>();
      PathfindingSystem = pathfindingSystem;
      this.heuristics = heuristics;
    }

    public void Search(bool skipBuild = false) {
      findPath();

      if (FoundPath && !skipBuild) {
        buildPath();
      }
    }

    private void findPath() {
      openNode(SearchParams.StartNode, 0, -1);
      while (SearchParams.OpenNodes.Count > 0) {
        int currentNode = getCheapestOpenNode();
        if (currentNode == SearchParams.TargetNode) {
          FoundPath = true;
          return;
        }
        closeNode(currentNode);
        foreach (Edge edge in PathfindingSystem.GetNode(currentNode).Edges) {
          handleEdge(edge, currentNode);
        }
      }
    }

    private void handleEdge(Edge edge, int currentNode) {
      int neighbour = edge.GetOtherEndID(currentNode);
      if (isClosed(neighbour) || PathfindingSystem.DeactivatedNodes.Contains(neighbour))
        return;

      if (alreadyOpen(neighbour)) {
        handleAlreadyVisitedNode(neighbour, currentNode, edge.TravelCost);
      } else {
        handleNewNode(neighbour, currentNode, edge.TravelCost);
      }
    }

    private void handleAlreadyVisitedNode(int node, int newParentNode, int edgeTravelCost) {
      var travelCost = SearchParams.TravelCost[node] + edgeTravelCost;
      if (travelCost < SearchParams.TravelCost[node]) {
        setParent(node, newParentNode);
        SearchParams.TravelCost[node] = travelCost;
        countTotalCost(node);
      }
    }

    private void handleNewNode(int newNode, int parentNode, int travelCost) {
      openNode(newNode, travelCost, parentNode);
      countHeuristics(newNode);
      countTotalCost(newNode);
    }

    private void buildPath() {
      int currentNode = SearchParams.TargetNode;
      while (currentNode != SearchParams.StartNode) {
        Path.Add(currentNode);
        currentNode = SearchParams.Parent[currentNode];
      }
      Path.Reverse();
    }

    private void openNode(int node, int travelCost, int parentNode) {
      SearchParams.OpenNodes.Add(node);
      countTravelCost(node, travelCost, parentNode);
      if (parentNode != -1)
        setParent(node, parentNode);
    }

    private void closeNode(int node) {
      SearchParams.OpenNodes.Remove(node);
      SearchParams.ClosedNodes.Add(node);
    }

    private void countHeuristics(int node) {
      SearchParams.Heuristic[node] = heuristics.count(PathfindingSystem.GetNode(node), this);
    }

    private void countTravelCost(int node, int travelCost, int parentNode) {
      SearchParams.TravelCost[node] =
        parentNode != -1 // special case - start node
        ? SearchParams.TravelCost[parentNode] + travelCost
        : travelCost;
    }

    private void countTotalCost(int node) {
      SearchParams.TotalCost[node] = SearchParams.TravelCost[node] + SearchParams.Heuristic[node];
    }

    private void setParent(int node, int parentNode) {
      SearchParams.Parent[node] = parentNode;
    }

    private int getCheapestOpenNode() {
      int node = -1;
      int currentValue = int.MaxValue;
      foreach (int nodeId in SearchParams.OpenNodes) {
        var travelCost = SearchParams.TravelCost[nodeId];
        if (travelCost < currentValue) {
          currentValue = travelCost;
          node = nodeId;
        }
      }
      return node;
    }

    private bool isClosed(int node) {
      return SearchParams.ClosedNodes.Contains(node);
    }

    private bool alreadyOpen(int node) {
      return SearchParams.OpenNodes.Contains(node);
    }
  }
}
