﻿using System;

namespace MonoPanda.UtilityClasses {
  public class Observer {
    private Delegate updateAction;

    public Observer(Delegate action) {
      updateAction = action;
    }

    public void Inform() {
      updateAction.DynamicInvoke();
    }

    public void Inform(params object[] @params) {
      updateAction.DynamicInvoke(@params);
    }
  }
}
