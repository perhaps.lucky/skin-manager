﻿using IniParser.Model;
using MonoPanda.UtilityClasses;

namespace MonoPanda.Configuration {
  public class StatesConfig : DefaultIniSectionLoader {
    public readonly string StatesJsonPath;
    public readonly string InitialStateId;

    public StatesConfig(IniData iniData) : base(iniData, "States") { }
  }
}