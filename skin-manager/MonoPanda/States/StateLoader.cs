﻿using MonoPanda.Multithreading;

namespace MonoPanda.States {
  public class StateLoader : ThreadRequestSystem {
    protected override bool Accepts(ThreadRequest request) {
      return request is StateRequest;
    }

    protected override void Process(ThreadRequest request) {
      StateRequest stateRequest = request as StateRequest;
      State state = stateRequest.State;
      if(stateRequest.Reinitialize && state.Initialized)
        state.Terminate();
      state.Initialize();
      state.Initialized = true;
    }
  }
}