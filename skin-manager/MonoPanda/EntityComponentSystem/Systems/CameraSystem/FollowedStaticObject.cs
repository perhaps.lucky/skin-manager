﻿using Microsoft.Xna.Framework;

namespace MonoPanda.ECS.Systems.Camera{
  public class FollowedStaticObject : FollowedObject {

    private Vector2 position;

    public FollowedStaticObject(Vector2 position) {
      this.position = position;
    }

    public override Vector2 GetPosition() {
      return position;
    }

    public override Vector2 GetSize() {
      return Vector2.Zero;
    }

    public override Vector2 GetVelocity() {
      return Vector2.Zero;
    }
  }
}
