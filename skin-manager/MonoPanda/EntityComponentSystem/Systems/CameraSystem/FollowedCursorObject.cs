﻿using Microsoft.Xna.Framework;
using MonoPanda.Input;
using MonoPanda.Utils;

namespace MonoPanda.ECS.Systems.Camera {
  public class FollowedCursorObject : FollowedObject {

    private ECS.Camera camera;
    private Entity rangeLimitEntity;
    private float rangeLimit;

    public FollowedCursorObject(ECS.Camera camera, Entity rangeLimitEntity = null, float rangeLimit = 300f) {
      this.camera = camera;
      this.rangeLimitEntity = rangeLimitEntity;
      this.rangeLimit = rangeLimit;
    }

    public override Vector2 GetPosition() {
      var cursorPosition = camera.ScreenToWorld(InputManager.GetMouseWindowPosition());
      if (rangeLimitEntity == null)
        return cursorPosition;
      else {
        float distance = Vector2.Distance(rangeLimitEntity.Position, cursorPosition);
        if (distance > rangeLimit) {
          return rangeLimitEntity.Position + VectorUtils.GetDirectionVector(rangeLimitEntity.Position, cursorPosition) * rangeLimit;
        }
        return cursorPosition;
      }
    }

    public override Vector2 GetSize() {
      return Vector2.Zero;
    }

    public override Vector2 GetVelocity() {
      return Vector2.Zero;
    }
  }
}
