﻿using Microsoft.Xna.Framework;

namespace MonoPanda.ECS.Systems.Camera {
  public abstract class FollowedObject {
    public abstract Vector2 GetPosition();
    public abstract Vector2 GetSize();
    public abstract Vector2 GetVelocity();
  }
}
