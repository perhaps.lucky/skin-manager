﻿using Microsoft.Xna.Framework;

using MonoPanda.Configuration;
using MonoPanda.ECS;
using MonoPanda.ECS.Systems.Camera;
using MonoPanda.GlobalTime;
using MonoPanda.Utils;

using System.Collections.Generic;
using System.Linq;

namespace MonoPanda.Systems {
  public class CameraSystem : EntitySystem {
    // Value of distance between camera position and expected center after which camera will start following
    public float FollowAfterDistance { get; set; } = 50;

    // Value of distance between camera position and expected center until which camera will follow
    public float FollowUntilDistance { get; set; } = 20;
    public float CameraSpeedMultiplier { get; set; } = 1f;

    public float SmoothZoomSpeed { get; set; } = 2f;

    // Distance to end of visible area that triggers auto zoom out if one of followed objects crosses it
    public float AutoZoomMargin { get; set; } = 10f;
    public float AutoZoomSpeed { get; set; } = 1f;

    public float SmoothZoomMax { get; set; } = -1;
    public float SmoothZoomMin { get; set; } = -1;

    public float SmoothZoomTarget { get; private set; } = 1f;
    private float autoZoomTarget = 1f;

    private Vector2 cameraMovement;
    private bool cameraFollowsPosition;
    private List<FollowedObject> followedObjects;

    private Vector2 maxFollowedObjectSize;

    public CameraSystem(string id, int updateInterval = -1) : base(id, updateInterval) {
      followedObjects = new List<FollowedObject>();
    }

    public override void Update() {
      if (followedObjects.Count > 0) {
        followObjects();
        zoomForFollowedObjects();
      }

      moveCamera();
      smoothZoom();
    }

    public CameraState GetCameraState() {
      return new CameraState(ECS.Camera.Position, ECS.Camera.Zoom);
    }

    public void JumpToCameraState(CameraState cameraState) {
      ECS.Camera.Position = cameraState.Position;
      ECS.Camera.Zoom = cameraState.Zoom;
    }

    public void GoTo(Vector2 position) {
      followedObjects.Clear();
      followedObjects.Add(new FollowedStaticObject(position));
      updateMaxFollowedObjectSize();
    }

    public void FollowEntity(Entity entity) {
      followedObjects.Clear();
      followedObjects.Add(new FollowedEntityObject(entity));
      updateMaxFollowedObjectSize();
    }

    public void AddFollowedObject(FollowedObject o) {
      followedObjects.Add(o);
      updateMaxFollowedObjectSize();
    }

    public void RemoveFollowedObject(FollowedObject o) {
      followedObjects.Remove(o);
      updateMaxFollowedObjectSize();
    }

    public void SmoothZoomTo(float value) {
      SmoothZoomTarget = value;
      verifySmoothZoomInAllowedRange();
    }

    public void SmoothZoomIn(float value) {
      SmoothZoomTarget += value;
      verifySmoothZoomInAllowedRange();
    }

    public void SmoothZoomOut(float value) {
      SmoothZoomTarget -= value;
      verifySmoothZoomInAllowedRange();
    }

    private void verifySmoothZoomInAllowedRange() {
      if (SmoothZoomTarget > SmoothZoomMax && SmoothZoomMax != -1) {
        SmoothZoomTarget = SmoothZoomMax;
      }

      if (SmoothZoomTarget < SmoothZoomMin && SmoothZoomMin != -1) {
        SmoothZoomTarget = SmoothZoomMin;
      }
    }

    private void followObjects() {
      Vector2 center = getCenterPosition();
      debugCenterRectangle(center);
      Vector2 velocity = getVelocity();
      followToPosition(center, velocity);
    }

    private void zoomForFollowedObjects() {
      var visibleArea = ECS.Camera.GetVisibleArea();
      visibleArea.Inflate(-(maxFollowedObjectSize.X + AutoZoomMargin), -(maxFollowedObjectSize.Y + AutoZoomMargin));
      if (!areAllObjectsInArea(visibleArea)) {
        autoZoomTarget -= AutoZoomSpeed * Time.ElapsedCalculated;
      }

      visibleArea.Inflate(-AutoZoomMargin, -AutoZoomMargin);
      if (areAllObjectsInArea(visibleArea)) {
        autoZoomTarget += AutoZoomSpeed * Time.ElapsedCalculated;
        if (autoZoomTarget > SmoothZoomTarget)
          autoZoomTarget = SmoothZoomTarget;
      }
    }

    private bool areAllObjectsInArea(Rectangle area) {
      foreach (FollowedObject o in followedObjects) {
        Rectangle objectRectangle = new Rectangle(
          (int)(o.GetPosition().X - o.GetSize().X / 2),
          (int)(o.GetPosition().Y - o.GetSize().Y / 2),
          (int)o.GetSize().X,
          (int)o.GetSize().Y);

        if (!area.Intersects(objectRectangle))
          return false;
      }

      return true;
    }

    private Vector2 getCenterPosition() {
      if (followedObjects.Count == 1) {
        return followedObjects[0].GetPosition();
      }

      var maxX = followedObjects.Max(o => o.GetPosition().X);
      var minX = followedObjects.Min(o => o.GetPosition().X);
      var maxY = followedObjects.Max(o => o.GetPosition().Y);
      var minY = followedObjects.Min(o => o.GetPosition().Y);

      return new Vector2(minX + (maxX - minX) / 2, minY + (maxY - minY) / 2);
    }

    private Vector2 getVelocity() {
      var objectsWithNonZeroVelocity = followedObjects.FindAll(o => o.GetVelocity() != Vector2.Zero);
      if (objectsWithNonZeroVelocity.Count == 0)
        return Vector2.Zero;

      var avX = objectsWithNonZeroVelocity.Average(o => o.GetVelocity().X);
      var avY = objectsWithNonZeroVelocity.Average(o => o.GetVelocity().Y);
      return new Vector2(avX, avY);
    }


    private void followToPosition(Vector2 position, Vector2 velocity) {
      var distance = Vector2.Distance(position, ECS.Camera.Position);
      cameraMovement = Vector2.Zero;
      if (distance > FollowAfterDistance)
        cameraFollowsPosition = true;

      if (cameraFollowsPosition)
        setMovementToPosition(distance, position, velocity);

      if (distance < FollowUntilDistance)
        cameraFollowsPosition = false;
    }

    private void setMovementToPosition(float distance, Vector2 position, Vector2 velocity) {
      var angle = VectorUtils.GetDirectionVector(ECS.Camera.Position, position);
      if (distance > FollowAfterDistance) {
        var fixedVelocity = fixVelocity(velocity, angle);
        cameraMovement = angle * distance + fixedVelocity;
      } else {
        // this gives a nice slow down effect when entity stops moving
        cameraMovement = angle * distance;
      }
    }

    // Prevents cases where velocity is opposite of angle movement,
    // which resulted in movement being slower that it should be.
    // Camera still moves faster in direction opposite to current velocity
    private Vector2 fixVelocity(Vector2 velocity, Vector2 angle) {
      float X;
      float Y;
      if (angle.X > 0) {
        X = velocity.X > 0 ? velocity.X : 0;
      } else {
        X = velocity.X < 0 ? velocity.X : 0;
      }

      if (angle.Y > 0) {
        Y = velocity.Y > 0 ? velocity.Y : 0;
      } else {
        Y = velocity.Y < 0 ? velocity.Y : 0;
      }

      return new Vector2(X, Y);
    }

    private void moveCamera() {
      ECS.Camera.Position += cameraMovement * Time.ElapsedCalculated * CameraSpeedMultiplier;
    }

    private void smoothZoom() {
      if (autoZoomTarget == ECS.Camera.Zoom)
        return;

      if (autoZoomTarget > ECS.Camera.Zoom) {
        ECS.Camera.Zoom += SmoothZoomSpeed * Time.ElapsedCalculated;
        if (ECS.Camera.Zoom >= autoZoomTarget) {
          ECS.Camera.Zoom = autoZoomTarget;
        }
      } else {
        ECS.Camera.Zoom += -SmoothZoomSpeed * Time.ElapsedCalculated;
        if (ECS.Camera.Zoom <= autoZoomTarget) {
          ECS.Camera.Zoom = autoZoomTarget;
        }
      }
    }

    private void debugCenterRectangle(Vector2 center) {
      if (Config.Debug.CameraSystemCenterRectangle)
        DrawUtils.DrawRectangle(ECS, new Rectangle((int)center.X - 5, (int)center.Y - 5, 10, 10), Color.Aquamarine);
    }

    private void updateMaxFollowedObjectSize() {
      var maxWidth = followedObjects.Max(o => o.GetSize().X);
      var maxHeight = followedObjects.Max(o => o.GetSize().Y);
      maxFollowedObjectSize = new Vector2(maxWidth, maxHeight);
    }

    public override string ToString() {
      return "%{LightGreen}% CameraSystem: \n"
             + "%{PaleGoldenrod}% FollowAfterDistance: %{White}% " + FollowAfterDistance + "\n"
             + "%{PaleGoldenrod}% FollowUntilDistance: %{White}% " + FollowUntilDistance + "\n"
             + "%{PaleGoldenrod}% CameraSpeedMultiplier: %{White}% " + CameraSpeedMultiplier + "\n"
             + "%{PaleGoldenrod}% SmoothZoomSpeed: %{White}% " + SmoothZoomSpeed + "\n"
             + "%{PaleGoldenrod}% AutoZoomMargin: %{White}% " + AutoZoomMargin + "\n"
             + "%{PaleGoldenrod}% AutoZoomSpeed: %{White}% " + AutoZoomSpeed;
    }
  }
}