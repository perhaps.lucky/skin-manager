﻿using Microsoft.Xna.Framework.Graphics;
using MonoPanda.UserSettings;

namespace MonoPanda {
  public class GameSettings {

    private static GameSettings instance;

    public bool IsCursorVisible { get; set; }
    public bool DrawCustomCursor { get; set; }
    public string CustomCursorContentId { get; set; }

    private GameSettings() { }

    public static GameSettings getInstance() {
      if (instance == null)
        instance = new GameSettings();

      return instance;
    }

    public static void SetFullscreen(bool fullscreen) {
      GameMain.GraphicsDeviceManager.IsFullScreen = fullscreen;
      Settings.Window.Fullscreen = fullscreen;
      GameMain.GraphicsDeviceManager.ApplyChanges();
    }

    public static void SetResolution(int width, int height) {
      GameMain.GraphicsDeviceManager.PreferredBackBufferWidth = width;
      GameMain.GraphicsDeviceManager.PreferredBackBufferHeight = height;
      Settings.Window.Width = width;
      Settings.Window.Height = height;
      GameMain.GraphicsDeviceManager.ApplyChanges();
    }

    public static void SetCustomCursorContentId(string contentId) {
      getInstance().CustomCursorContentId = contentId;
    }

    public static void SetCursorVisible(bool value) {
      var instance = getInstance();
      instance.IsCursorVisible = value;
      instance.updateSystemCursorState();
    }

    public static void SetDrawCustomCursor(bool value) {
      var instance = getInstance();
      instance.DrawCustomCursor = value;
      instance.updateSystemCursorState();
    }

    private void updateSystemCursorState() {
      GameMain.GetInstance().IsMouseVisible = IsCursorVisible ? !DrawCustomCursor : false;
    }
  }
}