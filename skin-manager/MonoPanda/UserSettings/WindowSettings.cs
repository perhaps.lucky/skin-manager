﻿using IniParser.Model;
using MonoPanda.UtilityClasses;

namespace MonoPanda.UserSettings {
  public class WindowSettings : SettingsIniSectionLoader {

    public int Width { get; set; }
    public int Height { get; set; }
    public bool Fullscreen { get; set; }

    public WindowSettings(IniData iniData) : base(iniData, "Window") { }
  }
}
