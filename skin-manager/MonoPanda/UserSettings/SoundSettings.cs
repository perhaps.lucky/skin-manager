﻿using IniParser.Model;
using MonoPanda.UtilityClasses;

namespace MonoPanda.UserSettings {
  public class SoundSettings : SettingsIniSectionLoader {

    public int SFXVolume { get; set; }
    public int MusicVolume { get; set; }

    public SoundSettings(IniData iniData) : base(iniData, "Sound") { }
  }
}
