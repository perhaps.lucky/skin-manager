﻿namespace MonoPanda.Repository {
  public abstract class RepositoryEntity {
    public string Id { get; set; }
    public virtual void Initialize() { }
  }
}
