set TEXTURE_PACKER_PATH="C:\Program Files\CodeAndWeb\TexturePacker\bin\TexturePacker"
set GIMP_PATH="C:\Program Files\GIMP 2\bin\gimp-console-2.10.exe"

set WORKING_PATH=.
set SPRITESHEET_KEY=%1

cd %WORKING_PATH%/
if not exist _flat (mkdir _flat)
if not exist _saved (mkdir _saved)

call %GIMP_PATH% -i -b "(plug-in-export-layers 1 1 \".png\" \"_saved\" \"\" 0)" -b "(gimp-quit 0)" %SPRITESHEET_KEY%.xcf

for /R "_saved/" %%i in (*.png) do move "%%i" "_flat/"

call %TEXTURE_PACKER_PATH% --texture-format png --opt RGBA8888 --format json-array --trim-sprite-names --max-width 1024 --algorithm Basic --extrude 0 --png-opt-level 0  --disable-auto-alias --trim-mode None --sheet exported/%SPRITESHEET_KEY%.png --data exported/%SPRITESHEET_KEY%.json _flat 


rmdir /s /q _flat
rmdir /s /q _saved


pause